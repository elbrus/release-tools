#!/usr/bin/python
# vim:set et ts=4 sw=4 ft=python:

import pickle

print "Loading data..."
packages = pickle.load(open('packages.updates.dump', 'r'))
source_mapping = pickle.load(open('source-mapping.updates.dump', 'r'))

# Add missing source -> source mappings.
for package in packages:
    if 'source' in packages[package] and package not in source_mapping:
        source_mapping[package] = package

# Calculate the reverse source mapping aka binary mapping.
binary_mapping = {}
for package in source_mapping:
    source_pkg = source_mapping[package]
    if source_pkg not in binary_mapping:
        binary_mapping[source_pkg] = []
    binary_mapping[source_pkg].append(package)

# Check for binNMUs.
print "Collecting binNMUs..."
binnmus = {}
for package in packages:
    # Package needs an entry in the source mapping.  Next look if
    # the source is present in p-u -> if not, it's a binNMU.
    # Then check if there is a source entry in the packages'
    # entry (if there is a binary package named after the source
    # package).
    if package in source_mapping and \
            (source_mapping[package] not in packages \
            or 'source' not in packages[source_mapping[package]]):
        if source_mapping[package] not in binnmus:
            binnmus[source_mapping[package]] = []
        binnmus[source_mapping[package]].append((package, packages[package]))

# Fetch sourceful uploads and put them into a dict with source package
# name as its key and a (version, binaries) tuple as its value.
print "Collecting sourceful uploads..."
sourcefuls = {}
for package in packages:
    if package in binnmus:
        continue
    if 'source' not in packages[package]:
        continue
    version = packages[package]['source']
    binaries = {}
    for binary in binary_mapping[package]:
        binaries[binary] = packages[binary]
    sourcefuls[package] = (version, binaries)

print "Writing out..."
pickle.dump(binary_mapping, open('binary-mapping.dump', 'w'))
pickle.dump(sourcefuls, open('sourcefuls.dump', 'w'))
pickle.dump(binnmus, open('binnmus.dump', 'w'))

