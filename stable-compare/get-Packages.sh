#!/bin/bash

SUITE="$1"

SECURITYBASE="http://security.debian.org"

: > /srv/release.debian.org/tmp/${SUITE}-Packages-security
: > /srv/release.debian.org/tmp/${SUITE}-Packages-proposed
: > /srv/release.debian.org/tmp/${SUITE}-Sources-security
: > /srv/release.debian.org/tmp/${SUITE}-Sources-proposed

for arch in $(dak admin s-a list-arch ${SUITE}); do
	url=${SECURITYBASE}/dists/${SUITE}/updates/main/binary-${arch}/Packages.gz
        wget $url -q -O - | gunzip >> /srv/release.debian.org/tmp/${SUITE}-Packages-security
	echo >> /srv/release.debian.org/tmp/${SUITE}-Packages-security
done

wget -q ${SECURITYBASE}/dists/${SUITE}/updates/main/source/Sources.gz -O - | gunzip > /srv/release.debian.org/tmp/${SUITE}-Sources-security

for arch in $(dak admin s-a list-arch ${SUITE}); do
	for file in \
	  /srv/ftp-master.debian.org/mirror/dists/${SUITE}-proposed-updates/main/binary-${arch}/Packages.gz \
	  /srv/ftp-master.debian.org/policy/dists/${SUITE}-new/main/binary-${arch}/Packages.gz \
	  /srv/ftp-master.debian.org/mirror/dists/${SUITE}/main/binary-${arch}/Packages.gz; do
		zcat $file >> /srv/release.debian.org/tmp/${SUITE}-Packages-proposed
		echo >> /srv/release.debian.org/tmp/${SUITE}-Packages-proposed
	done
done

zcat /srv/ftp-master.debian.org/mirror/dists/${SUITE}-proposed-updates/main/source/Sources.gz > /srv/release.debian.org/tmp/${SUITE}-Sources-proposed
zcat /srv/ftp-master.debian.org/policy/dists/${SUITE}-new/main/source/Sources.gz >> /srv/release.debian.org/tmp/${SUITE}-Sources-proposed
zcat /srv/ftp-master.debian.org/mirror/dists/${SUITE}/main/source/Sources.gz >> /srv/release.debian.org/tmp/${SUITE}-Sources-proposed
