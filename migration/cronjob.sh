#!/bin/sh

# cron job for testing.pl
# runs every hour on the hour to check for updated Debian data
# TODO trigger from britney...
# TODO make the update atomic...

set -e

# if we're running on a debian.org host, use the local SSL setup
dir=/etc/ssl/ca-debian
capath=""
test -d $dir && capath=" --capath $dir"

BASE=/srv/release.debian.org/tools/migration
UTF8=$BASE/utf8.txt
POINTS=$BASE/points.txt
SCRIPT=$BASE/testing.pl

WWW_OUT=/srv/release.debian.org/www/migration

cd $WWW_OUT
ln -sf $SCRIPT testing.txt

##

curl $capath --silent --compressed -O -z removals.html https://ftp-master.debian.org/removals.html

for what in sources packages
do
  for dist in unstable testing experimental
  do
    if [ "$what" = "sources" ]; then
      ftpfile="source/Sources.xz"
      mergetarget="Sources"
      greptarget="srcs"
      greppattern="^(Package:|Version:)"
    else
      ftpfile="binary-i386/Packages.xz"
      mergetarget="Packages"
      greptarget="pkgs"
      greppattern="^(Package|Provides|Version):"
    fi

    if [ "$dist" != "unstable" ]; then
      distprefix="$dist."
    else
      distprefix=""
      if [ "$what" = "sources" ]; then
        greppattern="^(Package|Depends|Build-Depends|Binary|Version|Maintainer|Architecture|Directory):"
      else
        greppattern="^(Filename|Package|Provides|Version|Source|Depends|Conflicts|Maintainer|Architecture):"
      fi
    fi

    rm -f "${distprefix}${mergetarget}" "${distprefix}${greptarget}"

    for component in main contrib non-free
    do
      filename="${distprefix}sources-${component}"
      if [ ! -L "$filename" ]; then
        ln -sf "/srv/ftp.debian.org/mirror/ftp-master/dists/$dist/main/$ftpfile" "${filename}.xz"
      fi
      xzcat "${filename}.xz" > "$filename"
      cat "$filename" >> "${distprefix}${mergetarget}"
      egrep "$greppattern" "${distprefix}${mergetarget}" > "${distprefix}${greptarget}"
    done
  done
done

# popularity contest
curl $capath --silent -O -z by_inst http://popcon.debian.org/by_inst.gz
if [ -s by_inst.gz ]
then
  gzip -qdf by_inst.gz
fi

# incoming.debian.org
curl $capath --silent -o incoming.html -z incoming.html http://incoming.debian.org/

# rebuild cache files?
if [ testing.pkgs -nt toplist.html -o testing.srcs -nt toplist.html -o pkgs -nt toplist.html -o srcs -nt toplist.html -o update_output.txt -nt toplist.html -o update_excuses.html -nt toplist.html -o testing.pl -nt toplist.html ]
then
  cp $UTF8 toplist.html
  $SCRIPT deps | sort -rn >> toplist.html
  cp $UTF8 oldest.html
  $SCRIPT age | sort -rn >> oldest.html
  cp $UTF8 points.html
  cat $POINTS >> points.html
  $SCRIPT points | sort -rn >> points.html
  cp $UTF8 stalls.html
  $SCRIPT stalls | sort -rn >> stalls.html
  cp $UTF8 stalls-nonew.html
  echo "<h2>Listing only packages with old versions in testing:</h2>" >> stalls-nonew.html
  $SCRIPT stalls-nonew | sort -rn >> stalls-nonew.html
  cp $UTF8 stalls-ready.html
  echo "<h2>Listing only packages past their waiting period:</h2>" >> stalls-ready.html
  $SCRIPT stalls-ready | sort -rn >> stalls-ready.html
  cp $UTF8 conflicts.html
  $SCRIPT conflicts >> conflicts.html
  cp $UTF8 olddeps.html
  $SCRIPT olddeps >> olddeps.html
  $SCRIPT accepted > accepted.html

  $SCRIPT obsolete > obsolete.html
  $SCRIPT >> obsolete.html

  rm -rf cache/*
  $SCRIPT mkcache
fi

# vim:set et ts=2 sw=2:

