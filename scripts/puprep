#!/bin/sh
#
# puprep: produce files useful when preparing for a point release
#
# (C) Copyright 2017-2018 Adam D. Barratt <adsb@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies:
# - dak
# - devscripts (for the "bts" command)
# - libsoap-lite-perl (for bug information fetching using "bts")

export LC_ALL=C

POINTREL="$1"
SUITE="$2"
PKGARCHES="$3"
CONTENTARCHES="$4"
SKIP="$5"

MAJORVER="$(echo $POINTREL | cut -d. -f1)"
if [ "$SUITE" = "stable" ]; then
	PUSUITE=proposed-updates
else
	PUSUITE="${SUITE}-proposed-updates"
fi

CODENAME="$(dak admin suite show ${SUITE} | awk '/^Codename: / {print $2}')"

BASEDIR=/srv/release.debian.org/www/proposed-updates/

cd ${BASEDIR}/${MAJORVER}
rm -rf ${POINTREL}/before
mkdir -p ${POINTREL}/before

cd ${POINTREL}

cp -a /srv/mirrors/debian/dists/${SUITE}/ before
rm -rf before/${SUITE}/main/installer-*

zcat before/${SUITE}/main/source/Sources.gz > Sources-before
for arch in $(echo "$PKGARCHES" | tr "," " ")
do
	zcat before/${SUITE}/main/binary-${arch}/Packages.gz > ${arch}-before
done

for arch in source $(echo "$CONTENTARCHES" | tr "," " ")
do
	zcat before/${SUITE}/main/Contents-${arch}.gz > Contents-${arch}-before
done

: > toskip.cs
: > toskip-debug.cs
if [ -n "$SKIP" ]; then
	dak ls -s ${PUSUITE} -f control-suite -S $(echo "$SKIP" | tr "," " ") | sort > toskip.cs
	dak ls -s ${PUSUITE}-debug -f control-suite -S $(echo "$SKIP" | tr "," " ") | sort > toskip-debug.cs
fi

dak control-suite -l ${SUITE} | sort > ${SUITE}.cs
dak control-suite -l ${PUSUITE} | sort > ${PUSUITE}.cs
(comm -3 ${PUSUITE}.cs toskip.cs ; cat ${SUITE}.cs) | sort > combined.cs

/srv/release.debian.org/tools/scripts/naive-heidi-dominate < combined.cs | sort > combined-dominated.cs

for pkg in $(cut -d" " -f2 ${BASEDIR}/${SUITE}_comments/REMOVALS)
do
	dak ls -s ${SUITE} -f control-suite -S $pkg
done | sort > removals.cs

for pkg in $(cut -d" " -f2 ${BASEDIR}/${SUITE}_comments/REMOVALS)
do
	dak ls -s ${SUITE}-debug -f control-suite -S $pkg
done | sort > removals-debug.cs

for item in $(sed -e "s/^#\([0-9]*\): \([^ ]*\) .*/\1_\2/" ${BASEDIR}/${SUITE}_comments/REMOVALS)
do
	bug=$(echo $item | cut -d_ -f1)
	pkg=$(echo $item | cut -d_ -f2)
	if ! grep "^${pkg} " ${SUITE}.cs"
	then
		echo "WARNING: package ${pkg} listed in REMOVALS appears not to exist in ${SUITE}" >&2
	fi
	subject=$(bts status $bug fields:subject | cut -f2- | sed -e "s/^RM: .* -- //")
	printf "dak rm -s %s -R -p -d %s -m '%s' %s\n" "$SUITE" "$bug" "$subject" "$pkg"
done | sort > removals.${CODENAME}

comm -3 combined.cs removals.cs > combined-removals.cs

/srv/release.debian.org/tools/scripts/naive-heidi-dominate < combined-removals.cs | sort > combined-removals-dominated.cs

dak ls -s ${PUSUITE} -f control-suite -S debian-installer | sort > ${CODENAME}-r0-additions.cs

if [ "${SUITE}" = "stable" ]
then
	eval $(dak admin c db-shell)
	for PROPSUITE in testing unstable
	do
		/srv/release.debian.org/tools/scripts/suitecomp ${PUSUITE} gt ${PROPSUITE} | sort | \
			grep -F -f toskip.cs -v | grep -F -f removals.cs -v > propups.${PROPSUITE}
		/srv/release.debian.org/tools/scripts/suitecomp ${PUSUITE}-debug gt ${PROPSUITE}-debug | sort | \
			grep -F -f toskip-debug.cs -v | grep -F -f removals-debug.cs -v > propups.${PROPSUITE}-debug
	done
fi
