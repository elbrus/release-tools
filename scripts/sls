#! /bin/sh
## sls: an alternative to `dak ls`.
#
# This program can be passed either source package names, or binary package
# names. In both cases, it will print a table with the source versions in the
# archive, a second table with the binary version for the given package (which
# may be empty), and a third table with the binary versions for other binary
# packages provided by the source. hurd-i386 binaries are ignored.
#
# If you pass the string "LIKE" as the first argument, before package names,
# the SQL LIKE operator will be used to search for packages, instead of an
# exact match.
#
# Copyright (c) 2009 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

set -e
set -u

if [ "${1-}" = "LIKE" ]; then
    OPERATOR="LIKE"
    shift
else
    OPERATOR="="
fi

if [ $# -ne 1 ]; then
    echo >&2 "Usage: `basename $0` PKG"
    exit 1
elif [ "$OPERATOR" = "LIKE" ]; then
    PKG="%${1}%"
else
    PKG="${1}"
fi

if [ -x "`which smart_pager 2>/dev/null`" ]; then
    SENSIBLE_PAGER="smart_pager"
fi

psql -q projectb <<EOF | ${SENSIBLE_PAGER-sensible-pager}
\pset footer off
--
-- Source versions table.
--
SELECT s.source, s.version, suite_name AS suite
FROM src_associations sa, suite su,
     (SELECT s.source, s.version, s.id
      FROM source s WHERE source $OPERATOR '$PKG'
      -- A direct JOIN here is slow, use UNION ALL (thanks, Sesse).
      UNION ALL SELECT s.source, s.version, s.id
      FROM source s JOIN binaries b ON s.id = b.source
      WHERE b.package = '$PKG') s
WHERE sa.source = s.id AND sa.suite = su.id
GROUP BY s.source, s.version, suite_name
-- The UNION seems to be discarding type information for the
-- "version" column; cast it back to a "debversion" when sorting.
ORDER BY s.source, s.version::debversion USING <, suite_name;

\qecho ===
\qecho

--
-- Binary versions (but only for the specified binary).
--
SELECT package AS binary, version, suite_name AS suite,
       space_separated_list(arch_string) AS arch
FROM
     -- We use a subquery now so that the arch list can be sorted
     (SELECT b.package, b.version, suite_name, arch_string
      FROM binaries b, bin_associations ba, suite su, architecture a
      WHERE b.package = '$PKG' AND arch_string != 'hurd-i386'
            AND ba.bin = b.id AND ba.suite = su.id AND a.id = b.architecture
      ORDER BY package, version, suite_name, arch_string) b
GROUP BY package, version, suite_name
ORDER BY package, version USING <, suite_name;

\qecho ===
\qecho

--
-- Other binary packages.
--
SELECT package AS other, version, suite_name AS suite,
       space_separated_list(arch_string) AS arch
FROM
     (SELECT b.package, b.version, suite_name, arch_string
      FROM (SELECT s.source, s.id
            FROM source s WHERE source $OPERATOR '$PKG'
            UNION SELECT s.source, s.id
            FROM source s JOIN binaries b ON s.id = b.source
            WHERE b.package $OPERATOR '$PKG') s,
           binaries b, bin_associations ba, suite su, architecture a
      WHERE b.package != '$PKG' AND arch_string != 'hurd-i386' AND
            b.source = s.id AND ba.bin = b.id AND ba.suite = su.id AND
            a.id = b.architecture
      ORDER BY arch_string) b
GROUP BY package, version, suite_name
ORDER BY package, version USING <, suite_name;
EOF
