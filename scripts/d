#! /usr/bin/python3
## vim:set et sw=4 encoding=utf-8 ft=python:

# (C) 2007, 2008 Philipp Kern <pkern@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * python3-six
#  * python3-apt

from __future__ import print_function

import sys
import os
import re
import subprocess
import errno
import apt_pkg
from six.moves.configparser import ConfigParser

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../lib/')
from debrelease.projectb import ProjectB
from debrelease.debdiff import DebDiffFactory
from debrelease.proposed import Deb822

ALWAYS_EXCLUDE = ['**/*.po', '**/*.pot']
NOT_IN_A_SUITE = '<none>'

def read_config():
    config_file = os.path.dirname(os.path.realpath(__file__)) + '/../config.ini'

    config = ConfigParser()
    if not config.read([ config_file ]):
        config = None
    return config

def write_to_pager(pager, *strs):
    try:
        for string in strs:
            pager.stdin.write(string)
    except IOError as exc:
        if exc.errno != errno.EPIPE:
            raise

def close_pager(pager):
    try:
        pager.stdin.close()
        pager.wait()
    except IOError as exc:
        if exc.errno != errno.EPIPE:
            raise


def can_use_colordiff():
    # Only use colors if we are being used interactively
    if not sys.stdout.isatty():
        return False
    try:
        with open('/dev/null', 'w') as nullfd:
            subprocess.check_call(["which", "colordiff"],
                                  stdout=nullfd, stderr=nullfd)
    except:
        # No colordiff => no colors ...
        return False
    use_colordiff = False
    if 'PAGER' in os.environ:
        pager = os.environ['PAGER']
    else:
        pager = '/usr/bin/pager'
    try:
        full_path = os.path.realpath(pager)
    except:
        full_path = '/usr/bin/sensible-pager'
    if (full_path.startswith('/bin/less') or
        full_path.startswith('/usr/bin/less')):
        # We are using less, it supports it with -R
        os.environ['PAGER'] = '%s -R' % full_path
        use_colordiff = True

    return use_colordiff


def invoke_pager():
    return subprocess.Popen(['/usr/bin/sensible-pager'], stdin=subprocess.PIPE)

def invoke_hint(*args):
    cmd = ['hint']
    cmd.extend(args)
    pipe = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    result = pipe.communicate()[0]
    return result.decode('utf-8').strip()

def invoke_grepexcuses(srcpkg, suite):
    if suite == 'testing-proposed-updates':
        srcpkg = '%s_tpu' % (srcpkg)
    pipe = subprocess.Popen(['grep-excuses', srcpkg], stdout=subprocess.PIPE)
    result = pipe.communicate()[0]
    return result.decode('utf-8').strip()

def usage():
    print("Usage: %s <srcpkg>" % sys.argv[0])
    print("       %s <srcpkg>/<suite>" % sys.argv[0])
    print("       %s <srcpkg>/<base-suite> <srcpkg>/<target-suite>" % sys.argv[0])
    return 1

def locate_hint_tool(argv0):
    try:
        # Check if hint is in PATH
        invoke_hint('help')
    except OSError:
        # Nope, locate hint.
        tool_dir = os.path.dirname(os.path.realpath(argv0))
        os.environ['PATH'] += ':' + tool_dir

def pipe_diff_through_cmd(diff, cmd):
    subproc = subprocess.Popen(cmd, stdin=subprocess.PIPE,
                           stdout=subprocess.PIPE)
    result, _ = subproc.communicate(diff)
    return result

def use_auto_filter():
    if sys.argv[0] == 'fd' or sys.argv[0].endswith('/fd'):
        return True
    if os.environ.get('D_AUTO_FILTER', 'false') == 'true':
        return True
    return False


def parse_package_reference(projectb, given_pkg, *, require_explicit_suite=False,
                            default_suite='unstable'):
    pkg = given_pkg
    suite = None
    version = None
    if '/' in given_pkg:
        pkg, suite_or_version = given_pkg.split('/')
        if projectb.is_valid_suite_name(suite_or_version):
            suite = suite_or_version
        else:
            ok, suite = projectb.check_version(pkg, suite_or_version)
            if not ok:
                print("E: %s is not a valid suite name (in argument %s)" % (suite, given_pkg),
                      file=sys.stderr)
                sys.exit(1)
            version = suite_or_version
            if suite is None:
                suite = NOT_IN_A_SUITE
    elif require_explicit_suite:
        print("E: %s is must be annotated with a valid suite name" % (given_pkg),
              file=sys.stderr)
        sys.exit(1)
    else:
        suite = default_suite
    return (pkg, suite, version)


def main():
    if len(sys.argv) < 2  or len(sys.argv) > 3:
        return usage()
    # Initialisation
    apt_pkg.init_system()
    config = read_config()
    projectb = ProjectB()
    debdiffer = DebDiffFactory(projectb, config)
    # Start working
    base_version = None
    target_version = None
    req_target_suite = None
    req_base_suite = 'testing'
    versions = {}

    target_src_pkg = sys.argv[1]
    base_src_pkg = None
    if len(sys.argv) == 3:
        orig_target = sys.argv[2]
        base_src_pkg, req_base_suite, base_version = parse_package_reference(projectb,
                                                                             sys.argv[1],
                                                                             require_explicit_suite=True)
        target_src_pkg, req_target_suite, target_version = parse_package_reference(projectb,
                                                                                   orig_target,
                                                                                   require_explicit_suite=True)
    else:
        orig_target = sys.argv[1]
        target_src_pkg, req_target_suite, target_version = parse_package_reference(projectb,
                                                                                   orig_target,
                                                                                   require_explicit_suite=False)
    suites = []
    if base_version is None:
        suites = [req_base_suite]

    if target_version is None:
        if req_target_suite is not None:
            suites.append(req_target_suite)
        else:
            suites.extend(['testing-proposed-updates', 'unstable'])

    locate_hint_tool(sys.argv[0])

    # Fetch versions (if needed)
    for suite in suites:
        versions[suite] = projectb.get_version(suite, target_src_pkg, False, True)

    # Sanity check: assert that the packages exist in the given suites
    if base_version is None and (req_base_suite == NOT_IN_A_SUITE or versions[req_base_suite] is None):
        # Base version is missing; something is wrong
        if target_version is None:
            if req_target_suite == NOT_IN_A_SUITE:
                print("E: Target version %s was not found in the database; maybe a typo or a binary package" % orig_target, file=sys.stderr)
            elif req_target_suite is None or versions[req_target_suite] is None:
                if req_target_suite is None:
                    req_target_suite = 'unstable'
                print('E: package %s neither in %s nor %s; maybe a typo or binary package?' % (target_src_pkg, req_base_suite, req_target_suite),
                      file=sys.stderr)
            else:
                print('E: package %s not in %s' % (target_src_pkg, req_base_suite), file=sys.stderr)
            return 1

    if base_version is None:
        base_version = versions[req_base_suite]
    if req_target_suite is not None or target_version is not None:
        target_suite = req_target_suite
        if target_version is None:
            target_version = versions[target_suite]
        print('I: using version %s from %s' % (target_version, target_suite),
                                file=sys.stderr)
    else:
        # Fetch version from tpu if there is a version available there,
        # otherwise take the version from unstable.
        if (versions['testing-proposed-updates'] is None
            or apt_pkg.version_compare(versions['testing-proposed-updates'],
                                       versions['testing']) <= 0):
            target_suite = 'unstable'
            target_version = versions['unstable']
        else:
            print('I: using version %s from testing-proposed-updates'
                  % versions['testing-proposed-updates'],
                  file=sys.stderr)
            target_suite = 'testing-proposed-updates'
            target_version = versions['testing-proposed-updates']

    # Identical versions do not need an action.
    if base_version == target_version:
        print('I: versions identical, nothing to diff', file=sys.stderr)
        return 0
    # Generate a header.
    header = "Base version: %s_%s from %s\nTarget version: %s_%s from %s\n\n" % \
           (target_src_pkg, base_version, req_base_suite, target_src_pkg, target_version, target_suite)
    header = header.encode('utf-8')
    # Generate the diff.
    debdiff = debdiffer.create_pool_pool_debdiff(
        target_src_pkg, base_version, target_version)

    # Provide these on the pager, so that review can be aborted if the package
    # is already unblocked.
    existing_hints = invoke_hint('grep', '/^%s$/' % re.escape(target_src_pkg))

    if existing_hints:
        existing_hints = 'Hints in place:\n' + existing_hints + '\n\n'
        need_udeb_unblock = (re.search('^\s*block-udeb\s', \
                                 existing_hints, re.MULTILINE) is not None)
    else:
        existing_hints = 'No hints in place.\n\n'
        need_udeb_unblock = False
    existing_hints = existing_hints.encode('utf-8')

    excuses = 'Excuses:\n\n%s\n\n' % (invoke_grepexcuses(target_src_pkg, target_suite))
    excuses = excuses.encode('utf-8')

    use_colordiff = can_use_colordiff()

    # Start up PAGER to display the diff
    pager = invoke_pager()
    if use_auto_filter():
        filterdiff_cmd = ['filterdiff']
        for expr in ALWAYS_EXCLUDE:
            filterdiff_cmd.append('-x')
            filterdiff_cmd.append(expr)
        the_diff = pipe_diff_through_cmd(debdiff.diff, filterdiff_cmd)

        filtered_with = "Filter applied (not reflected in the diffstat):\n  %s\n\n" % (
            ' '.join(filterdiff_cmd))
    else:
        the_diff = debdiff.diff
        filtered_with = ''
    filtered_with = filtered_with.encode('utf-8')

    if use_colordiff:
        the_diff = pipe_diff_through_cmd(the_diff, ['colordiff'])
    write_to_pager(pager, header, existing_hints, excuses, filtered_with,
                   debdiff.diffstat, b"\n", the_diff)
    close_pager(pager)

    if req_base_suite != 'testing' or (req_target_suite is not None
                                       and req_target_suite not in ('unstable', 'testing-proposed-updates')):
        print("Can only generate hints for versions from unstable and testing-proposed-updates to testing")
    else:
        print("Hints needed:")
        if target_suite != 'testing-proposed-updates':
            print(invoke_hint('unblock', target_src_pkg))
        else:
            print(invoke_hint('approve', target_src_pkg))
        if need_udeb_unblock:
            print("# XXXX: Confirm with d-i RM")
            if target_suite != 'testing-proposed-updates':
                print(invoke_hint('unblock-udeb', target_src_pkg))
            else:
                print("unblock-udeb %s/%s" % (target_src_pkg, target_version))
    return 0

if __name__ == '__main__':
    sys.exit(main())

