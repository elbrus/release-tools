#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Generate a list of `wb nmu` commands from a Packages stream.

Use grep-dctrl/grep-archive to obtain a list of stuff to binNMU, and pipe the
result to this program. The output can be piped out to scripts/wb on raff.

Give a changelog with -m, and set dep-waits with -d and --dw-arches.
"""

import re
import sys
import optparse

import apt_pkg
apt_pkg.init()

##

def main():
    options, args = parse_options()

    if not args or args[0] == '-':
        pkgs = sys.stdin
    else:
        pkgs = file(args[0])

    binNMUs = {} # { src1: (srcver1, set([ arch1, ... ])), ... }
    archall = {} # { src1: set([ archall1, ... ]), ... }
    try:
        parser = apt_pkg.TagFile(pkgs)
        step = parser.step
        section = parser.section
    except AttributeError, e:
        parser = apt_pkg.ParseTagFile(pkgs)
        step = parser.Step
        section = parser.Section

    while step():
        src = section.get('Source') or section['Package']

        if ' ' in src:
            m = re.match(r'(\S+) \((\S+)\)$', src)
            src = m.group(1)
            srcver = m.group(2)
        else:
            srcver = section['Version']

        if src not in binNMUs:
            binNMUs[src] = (srcver, set())
        else:
            # If we receive a package with an earlier version, we continue. If
            # we receive a package with a newer version, we drop our binNMUs
            # for earlier versions. Note that we may still have out of date
            # version here, for non-dinstalled stuff, but `wb` will take care
            # of those.
            c = apt_pkg.version_compare(binNMUs[src][0], srcver)
            if c > 0:
                continue
            elif c < 0:
                binNMUs[src] = (srcver, set())

        arch = section['Architecture']

        if arch != 'all':
            binNMUs[src][1].add(arch)
        else:
            archall.setdefault(src, set()).add(section['Package'])

    if archall:
        print >>sys.stderr, \
            'W: the following arch: all packages were found/skipped:'
        for src, pkgs in archall.iteritems():
            print >>sys.stderr, '   %s: %s' % (src, ' '.join(pkgs))
            del binNMUs[src] # do not nmu even if it has arch: any pkgs.

    msg = options.message
    dw = options.dep_wait

    for pkg, (ver, arches) in binNMUs.iteritems():
        print 'nmu %s_%s . %s . -m %r' % (pkg, ver, ' '.join(sorted(arches)), msg)
        dw_arches = arches & options.dw_arches
        if dw and dw_arches:
            print 'dw %s_%s . %s . -m %r' % (pkg, ver, ' '.join(sorted(dw_arches)), dw)

##

def parse_options():
    p = optparse.OptionParser()

    p.add_option('-m', '--message')
    p.add_option('-d', '--dep-wait')
    p.add_option('--dw-arches')

    p.set_defaults(message='', dw_arches=set())

    opts, args = p.parse_args()

    if opts.dw_arches:
        opts.dw_arches = set(re.split(r'[,\s]+', opts.dw_arches))

    if not opts.message:
        print >>sys.stderr, 'W: no changelog given.'

    return opts, args
##

if __name__ == '__main__':
    main()
