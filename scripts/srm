#! /bin/bash
#
# srm: automate some common tasks performed by Stable Release Managers
#
# (C) Copyright 2016-2018 Adam D. Barratt <adsb@debian.org>
# (C) Copyright 2017 Cyril Brulebois <kibi@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

PROGNAME="$(basename $0)"
CONFIG="$(dirname $0)"/config.ini

usage() {
    echo "Usage: ${PROGNAME} (accept|reject) (pu|opu) <comment filename>"
}

consistency_check() {
	queue="$1"
	comment_path="$2"
	comment_file="$3"
	action="$4"

	# Check queue name and queue path:
	case "$queue" in
		pu)
			queue_path="$(realpath $comment_path/../stable_comments)"
			queue_name=proposed-updates
			dist=stable
		;;
		opu)
			queue_path="$(realpath $comment_path/../oldstable_comments)"
			queue_name=oldstable-proposed-updates
			dist=oldstable
		;;
		*)
			echo "${PROGNAME}: Unsupported queue: $queue" >&2
			exit 1
		;;
	esac
	if [ "$queue_path" != "$comment_path" ]; then
		echo "${PROGNAME}: Incorrect queue: $queue" >&2
		echo "$queue comment files must be in $queue_path" >&2
		echo "Specified comment file is in $comment_path" >&2
		exit 1
	fi

	# Check version number:
	version="$(echo $comment_file|sed 's/.*_//')"
	case "$version" in
		*+deb*u* | *~deb*u*)
			version_major="$(echo $version|sed 's/.*[+~]deb\(.*\)u.*/\1/')"
			dist_major=$(sed -n "/^\[$dist\]$/,/^$/p" $CONFIG | awk '/^version/ {print $3}')
			codename=$(sed -n "/^\[$queue_name\]$/,/^$/p" $CONFIG | awk '/^codename/ {print $3}')
			if [ "$version_major" != "$dist_major" ] && [ "$action" = "accept" ]; then
				echo "${PROGNAME}: Invalid +debXuY / ~debXuY suffix" >&2
				echo "Target version $version has major $version_major" >&2
				echo "Target distribution $dist has major $dist_major" >&2
				exit 1
			fi
		;;
	esac
}

if [ $# != 3 ]
then
	usage
	exit 1
fi

ACTION="${1^^}"
QUEUE="$2"
_COMMENT_FILENAME="$3"

if [ ! -r "${_COMMENT_FILENAME}" ]
then
	echo "${PROGNAME}: File \"${_COMMENT_FILENAME}\" not found; exiting" >&2
	exit 1
fi

COMMENT_PATH="$(dirname $(realpath ${_COMMENT_FILENAME}))"
COMMENT_FILENAME="$(basename $(realpath ${_COMMENT_FILENAME}))"

consistency_check "$QUEUE" "$COMMENT_PATH" "$COMMENT_FILENAME" "$ACTION"

[[ ${COMMENT_FILENAME} =~ ^(.*)_(.*) ]] || exit 1
PACKAGE="${BASH_REMATCH[1]}"
VERSION="${BASH_REMATCH[2]}"

if [[ ${PACKAGE} =~ ^(ACCEPT|ACCEPTED|REJECT|REJECTED). ]]
then
	echo "${PROGNAME}:  File \"${_COMMENT_FILENAME}\" appears to have already been processed; exiting" >&2
	exit 1
fi

COMMENT_ACTION="$(head -n1 "${_COMMENT_FILENAME}")"
_COMMENT_REASON="$(tail -n+2 "${_COMMENT_FILENAME}" | head -n1)"
[[ ${_COMMENT_REASON} =~ ^(DSA ([0-9]*) )?${PACKAGE}\ -\ (.*) ]]
DSA_REF="${BASH_REMATCH[2]}"
if [ -n "${DSA_REF}" ]
then
	COMMENT_REASON="${DSA_REF}"
else
	COMMENT_REASON="${BASH_REMATCH[3]}"
fi

_COMMENT_BUG="$(tail -n1 "${_COMMENT_FILENAME}")"
[[ ${_COMMENT_BUG} =~ ^Bug#([0-9]*)( *)$ ]]
BUG_REF="${BASH_REMATCH[1]}"
if [ -z "${BUG_REF}" ]
then
	BUG_REF=$(sed -ne "s/ //g; s/^pu-bug://p" "${_COMMENT_FILENAME}")
fi

case "${ACTION}" in
	ACCEPT) if [[ "${COMMENT_ACTION}" != "OK" ]]
		then
			echo "${PROGNAME}: acceptance requested but comment file does not start \"OK\"" >&2
			exit 1
		fi
		;;
	REJECT) if [[ "${COMMENT_ACTION}" != "NOTOK" ]]
		then
			echo "${PROGNAME}: rejection requested but comment file does not start \"NOTOK\"" >&2
			exit 1
		fi
		;;
esac

if [ "$(id -nu)" != "release" ]
then
       SUDO="sudo -u release"
else
       SUDO=""
fi

if ${SUDO} "$(dirname $0)/pu-comment" "${ACTION}" "${QUEUE}" "${PACKAGE}" "${VERSION}" "${COMMENT_REASON}"
then
	mv "${_COMMENT_FILENAME}" "${COMMENT_PATH}/${ACTION}.${COMMENT_FILENAME}"

	if [ -n "$BUG_REF" ] &&  [ "${ACTION}" = "ACCEPT" ]
	then
		MAIL_FROM_OPT=""
		MAIL_FROM_ADDR=""
		if [ -n "$DEBFULLNAME" ] && [ -n "$DEBEMAIL" ]
		then
			MAIL_FROM_OPT=-r
			MAIL_FROM_ADDR="$DEBFULLNAME <$DEBEMAIL>"
		elif [ -n "$DEBEMAIL" ]
		then
			MAIL_FROM_OPT=-r
			MAIL_FROM_ADDR=$DEBEMAIL
		fi

		mail -s "${PACKAGE} ${VERSION} flagged for acceptance" ${BUG_REF}@bugs.debian.org -c ${BUG_REF}-submitter@bugs.debian.org $MAIL_FROM_OPT "$MAIL_FROM_ADDR" << EOT
Control: tags -1 + pending

Hi,

The upload referenced by this bug report has been flagged for acceptance into the proposed-updates queue for Debian ${codename}.

Thanks for your contribution!

Upload details
==============

Package: ${PACKAGE}
Version: ${VERSION}

Explanation: ${COMMENT_REASON}
EOT
	fi
fi
