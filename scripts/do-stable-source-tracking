#!/bin/bash

FTPDIR=/srv/ftp.debian.org/mirror/ftp-master
DATADIR=$(dirname $0)/../data

usage() {
	echo Usage: $0 suite
}

cleanup() {
	rm -f $TEMPDIR/{UDEB,HEIDI,SOURCE}-LIST
	rmdir $TEMPDIR
}

TEMPDIR=$(mktemp -d)
trap cleanup EXIT

if [ $# -ne 1 ]
then
	usage
	exit 1
fi

SUITE="$1"

HEIDIFILE=$DATADIR/${SUITE}-r0.heidi
if [ ! -f $HEIDIFILE ]
then
	echo E: $HEIDIFILE does not exist, please touch it at least.
	exit 1
fi

if [ "$SUITE" != "stable" ]
then
	PU=$SUITE-proposed-updates
else
	PU=proposed-updates
fi

# Get the current suite content.
dak ls -f heidi -s $SUITE -r '.*' > $TEMPDIR/HEIDI-LIST
dak ls -f heidi -s $PU -r '.*' >> $TEMPDIR/HEIDI-LIST
sort < $TEMPDIR/HEIDI-LIST | sponge $TEMPDIR/HEIDI-LIST
./naive-heidi-dominate < $TEMPDIR/HEIDI-LIST | sponge $TEMPDIR/HEIDI-LIST

# Parse the installer manifests to retrieve all udebs used in the current
# set of initrds.  The file format is: tab in the first column if a udeb
# is listed thereafter; char in the first column to name the initrd image
# in question.
grep -h -v -e '^\w' $FTPDIR/dists/$PU/main/installer-*/current/images/MANIFEST.udebs | \
	sed -e 's/^.//g' | \
	sort | uniq > $TEMPDIR/UDEB-LIST

echo INITRD MEMBERS WITHOUT SOURCE IN $SUITE
echo ====================================================================
# Check which udebs are currently missing in the suite.
(while read line; do grep -q "$line" $TEMPDIR/HEIDI-LIST || echo $line; done) < $TEMPDIR/UDEB-LIST

echo
echo SOURCES OF INITRD MEMBERS IN $SUITE
echo ====================================================================
# For all udebs currently present in the suite, fetch the corresponding source
# package entry.
(while read line; do grep -q "$line" $TEMPDIR/HEIDI-LIST && echo $line; done) < $TEMPDIR/UDEB-LIST | \
	(while read line; do echo "SELECT source.source, source.version, 'source' FROM source JOIN binaries ON binaries.source = source.id JOIN src_associations_bin ON src_associations_bin.source = source.id JOIN suite ON suite.id = src_associations_bin.suite JOIN architecture ON binaries.architecture=architecture.id WHERE binaries.package='$(echo $line | awk '{print $1}')' AND binaries.version='$(echo $line | awk '{print $2}')' AND architecture.arch_string='$(echo $line | awk '{print $3}')' AND suite.suite_name IN ('$SUITE', '$PU') LIMIT 1;" | \
	psql -F ' ' -At service=projectb; done) | sort | uniq

echo
echo MANUAL ENTRIES
echo ====================================================================
# Some packages cannot be deduced automatically.
cat $HEIDIFILE

