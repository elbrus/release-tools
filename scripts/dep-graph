#! /usr/bin/python3
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Make a graph of dependencies between/within a set of packages.

These graphs are best converted to images with twopi and not dot.
"""

# FIXME --source only works with --print-leaves
from __future__ import print_function

import re
import sys
import optparse
import subprocess

import apt_pkg

apt_pkg.init()

##

def main():
    opts, args = parse_options()
    pkgs = set([ re.sub(r',', '', pkg) for pkg in args ])

    cmd = [ 'grep-archive', '%s:ALL:%s' % (opts.suite, opts.arch),
            '-P', '-e', '^(%s)$' % '|'.join(pkgs), '-s' 'Package,Source,Depends' ]

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)

    ##

    edges = []
    srcpkg = {}
    leaves = set(pkgs)
    parser = apt_pkg.TagFile(p.stdout)
    step = parser.step
    section = parser.section

    while step():
        if 'Depends' not in section:
            continue

        thispkg = section['Package']

        if opts.source and section['Source']:
            srcpkg[thispkg] = section['Source']

        for dep in apt_pkg.parse_depends(section['Depends']):
            # TODO color 2+ alternatives differently
            for alternative in dep:
                pkg, ver, rel = alternative
                if pkg in pkgs:
                    leaves.discard(thispkg)
                    string = '"%s" -> "%s"' % (thispkg, pkg)
                    if ver and rel:
                        string += ' [label="%s %s"]' % (rel, ver)
                    edges.append(string + ';')

    if opts.print_leaves:
        if not opts.source:
            print('\n'.join(sorted(leaves)))
        else:
            d = {}
            for p in leaves:
                d.setdefault(srcpkg.get(p, p), set()).add(p)
            for src, pkgs in sorted(d.items()):
                print('%s (%s)' % (src, ' '.join(sorted(pkgs))))
    else:
        if opts.file:
            f = open(opts.file, 'w')
        else:
            f = sys.stdout

        with f:
            f.write('''digraph dependencies {
overlap=false;
node [color="#CCCCCC" fontcolor="#CCCCCC"];
edge [color="#CCCCCC" fontcolor="#CCCCCC"];
''')
            f.write('%s\n' % ( '\n'.join([
                '"%s" [color=black fontcolor=black];' % pkg for pkg in leaves ])))
            f.write('%s\n' % '\n'.join(edges))
            f.write('}\n')

##

def parse_options():
    p = optparse.OptionParser(usage='%s [options] pkg1 pkg2 [...]')

    p.add_option('-o', dest='file')
    p.add_option('-a', dest='arch')
    p.add_option('-s', dest='suite')
    p.add_option('--source', action='store_true',
            help='use source package names')
    p.add_option('--print-leaves', action='store_true',
            help="just print packages that don't depend on the rest")

    p.set_defaults(suite='testing', arch='i386')

    options, args = p.parse_args()

    if not args:
        p.error('need a list of packages')

    return options, args

##

if __name__ == '__main__':
    main()
