## vim:set noet ts=4 sw=4:
#
# debdiff.py: ease package diffing
#
# (C) 2007-2008 Philipp Kern <pkern@debian.org>
# Copyright 2011-2017 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * python-debian | python-deb822

import os, shutil, tempfile
import gzip
import logging
from io import open

from subprocess import Popen, PIPE, STDOUT

from debian.deb822 import Deb822
from debrelease import create_symlinks, FileNotFoundException

DEFAULT_POOL_LOCATION = '/srv/ftp.debian.org/mirror/ftp-master/pool'

class DebDiffFactory(object):
    def __init__(self, projectb, config = None, section = None):
        self.projectb = projectb
        self.logger = logging.getLogger('debrelease.debdiff')
        self.logger.addHandler(logging.NullHandler())
        if config is None:
            self.pool = DEFAULT_POOL_LOCATION
        else:
            self.pool = config.get('dak', 'pool')
            self.extra_pools = config.get('dak', 'extra_pools')
            if section:
                self.section = section
                self.queue_directory = config.get(section, 'directory')
                self.policy_suite = config.get(section, 'policy_suite', fallback=None)
                self.suite = config.get(section, 'base_suite')
                self.overlay_suite = section
                self.diff_output = config.get(section, 'diff_output')
                if not os.path.exists(self.diff_output):
                    os.mkdir(self.diff_output)
                assert os.path.isdir(self.diff_output)

    def create_pool_pool_debdiff(self, srcpkg, base_version, target_version):
        return DebDiff(
            pool = self.pool,
            extra_pools = self.extra_pools,
            srcpkg = srcpkg,
            base_version = base_version,
            target_version = target_version,
            source_file = self.projectb.get_dsc_file(srcpkg, base_version),
            target_file = self.projectb.get_dsc_file(srcpkg, target_version),
            projectb = self.projectb,
            )

    def create_pool_file_debdiff(self, srcpkg, base_version, target_file,
            target_version):
        return DebDiff(
            pool = self.pool,
            extra_pools = self.extra_pools,
            srcpkg = srcpkg,
            base_version = base_version,
            target_version = target_version,
            source_file = self.projectb.get_dsc_file(srcpkg, base_version),
            target_file = target_file,
            projectb = self.projectb,
            )

    def create_pool_debdiff(self, srcpkg, base_component, base_version,
            target_component, target_version):
        return DebDiff(
            pool = self.pool,
            extra_pools = self.extra_pools,
            srcpkg = srcpkg,
            base_component = base_component,
            base_version = base_version,
            target_component = target_component,
            target_version = target_version,
            target_in_pool = True,
            projectb = self.projectb,
            )

    def create_debdiff(self, srcpkg, version, against_overlay=False):
        """This method creates a debdiff object which gives, when converted
        to a string, a diff between the base revision determined by
        the suite the factory is created for and the version you pass
        into this method.

        By default the diff will be made against the current version in
        the stable suite, but it could optionally be done against the
        overlay proposed-updates suite instead, if against_overlay is
        passed holding True.

        If the version passed is equal to the version in p-u it will
        be diffed against stable, too, and it automatically adapts to
        the other location of the target file (i.e. in the pool instead
        of the queue directory).

        If you try to diff two same versions it will return None, too,
        instead of creating an empty diff object.  Thus if against_overlay
        is specified and you diff against p-u it will bail out for
        this very reason."""
        overlay_version = self.projectb.get_version(self.overlay_suite, srcpkg)
        stable_version = self.projectb.get_version(self.suite, srcpkg)
        if against_overlay:
            base_version = overlay_version
        else:
            base_version = stable_version
        component = self.projectb.get_component(self.suite, srcpkg)
        # Force overlay if not in base.
        if base_version is None:
            base_version = overlay_version
        # Retrieve component from overlay if necessary.
        if component is None:
            component = self.projectb.get_component(self.overlay_suite, srcpkg)
        if version == stable_version:
            # Diffing against the base suite won't work.
            return None
        if base_version == version:
            return None
        return DebDiff(
            pool = self.pool,
            extra_pools = self.extra_pools,
            component = component,
            queue_directory = self.queue_directory,
            target_in_pool = (overlay_version == version or self.policy_suite is not None),
            srcpkg = srcpkg,
            base_version = base_version,
            target_version = version,
            overlay_version = overlay_version,
            stable_version = stable_version,
            projectb = self.projectb,
            )

    def create_diff_file(self, srcpkg, version, against_overlay=False,
                         compressed=False):
        filename = "%s/%s_%s.debdiff" % (self.diff_output, srcpkg, version)
        if os.path.exists(filename):
            return None
        if compressed:
            filename = "%s.gz" % (filename)
            if os.path.exists(filename):
                return None
        diffobj = self.create_debdiff(srcpkg, version, against_overlay)
        if diffobj is None:
            return None
        diffobj.create_diff_file(filename, compressed)
        return filename

class DiffFailedException(Exception):
    def __init__(self, base_file, target_file, error):
        self.base_file, self.target_file, self.error = \
            base_file, target_file, error
    def __str__(self):
        return "DebDiff(%s, %s) failed:\n%s" % (self.base_file,
                                                self.target_file, self.error)

class DebDiff(object):
    temp_dir = None

    def __init__(self, pool, srcpkg, target_version, base_version,
                 projectb=None, source_file=None, component=None,
                 base_component=None, target_component=None, target_file=None,
                 target_in_pool=False, queue_directory=None,
                 stable_version=None, overlay_version=None, extra_pools=None):
        self.projectb = projectb
        self.logger = logging.getLogger('debrelease.debdiff')
        self.logger.addHandler(logging.NullHandler())
        self.pool = pool
        if extra_pools is None:
            self.extra_pools = []
        else:
            self.extra_pools = extra_pools.split(' ')
        self.base_version = base_version
        self.target_version = target_version
        self.srcpkg = srcpkg
        self.version_info = {}
        if stable_version is not None:
            self.version_info['stable'] = stable_version
        if overlay_version is not None:
            self.version_info['overlay'] = overlay_version
        self.temp_dir = tempfile.mkdtemp()

        if source_file is None:
            if component is not None:
                base_component = component
                target_component = component
            else:
                assert base_component is not None
                if target_file is None:
                    assert target_component is not None
            source_file = '%s/%s_%s.dsc' % (self._pool_pathname(base_component,
                                            srcpkg), srcpkg, base_version)
        # The base file is always in the pool.
        self.base_file = '%s/%s' % (pool, source_file)
        # The target file maybe in the pool if the caller says so.
        if target_file is not None:
            self.target_file = target_file
        elif target_in_pool:
            self.target_file = '%s/%s_%s.dsc' % (
                self._pool_pathname(target_component, srcpkg),
                srcpkg, target_version)
        else:
            self.target_file = '%s/%s_%s.dsc' % (queue_directory,
                srcpkg, target_version)
        if self.target_file[0] != '/':
            # If the target file is specified relatively, add the pool
            # location in front of it.
            for pool_path in [self.pool] + self.extra_pools:
                path = os.path.join(pool_path, self.target_file)
                if os.path.exists(path):
                    self.target_file = path
                    break
        # Now assert that both are present.
        if not os.path.exists(self.base_file):
            raise FileNotFoundException(self.base_file)
        if not os.path.exists(self.target_file):
            raise FileNotFoundException(self.target_file)

    def __del__(self):
        if self.temp_dir is not None:
            shutil.rmtree(self.temp_dir)

    def _pool_pathname(self, component, srcpkg):
        # libs are split in the pool, so they need to be special-cased
        if srcpkg[0:3] == 'lib':
            path = '%s/%s' % (srcpkg[0:4], srcpkg)
        else:
            path = '%s/%s' % (srcpkg[0], srcpkg)
        # add the component
        return '%s/%s' % (component, path)

    def _invoke_debdiff(self):
        if self.base_file is None or self.target_file is None:
            return None

        self.logger.info("Generating debdiff for %s vs %s", self.target_file, self.base_file)
        self.logger.debug("Generating symlink farms")
        create_symlinks(self.base_file, self.temp_dir,
                        [self.pool] + self.extra_pools, self.projectb)
        create_symlinks(self.target_file, self.temp_dir,
                        [self.pool] + self.extra_pools, self.projectb)

        new_base_file = os.path.join(self.temp_dir,
                                     os.path.basename(self.base_file))
        new_target_file = os.path.join(self.temp_dir,
                                       os.path.basename(self.target_file))
        self.logger.debug("Running debdiff")
        diff = Popen(['debdiff', new_base_file, new_target_file],
                     stdout=PIPE, stderr=STDOUT, close_fds=True)
        output = diff.communicate()
        if diff.returncode not in [0, 1]:
            raise DiffFailedException(self.base_file, self.target_file,
                                      output[0].strip())
        return output[0]

    def __str__(self):
        return self.diff

    def write_to_file(self, file):
        file.write(self.diff)

    @property
    def diff(self):
        if not hasattr(self, '_diff'):
            self._diff = self._invoke_debdiff()
        return self._diff

    @property
    def diffstat(self):
        if not hasattr(self, '_diffstat'):
            self.logger.debug("Generating diffstat")
            diffstat = Popen(['diffstat'], stdin=PIPE, stdout=PIPE,
                             stderr=STDOUT, close_fds=True)
            diffstat.stdin.write(self.diff)
            (stdout, stderr) = diffstat.communicate()
            self._diffstat = stdout
        return self._diffstat

    def _print_header(self, f):
        if 'stable' in self.version_info:
            f.write(("Version in base suite: %s\n" %  self.version_info['stable']).encode('ascii'))
        if 'overlay' in self.version_info:
            if self.version_info['overlay'] is None:
                version = "(not present)"
            else:
                version = self.version_info['overlay']
            f.write(("Version in overlay suite: %s\n" % version).encode('ascii'))
        f.write(b"\n")
        f.write(("Base version: %s_%s\n" % (self.srcpkg, self.base_version)).encode('ascii'))
        f.write(("Target version: %s_%s\n" % (self.srcpkg, self.target_version)).encode('ascii'))
        f.write(("Base file: %s\n" % (self.base_file)).encode('ascii'))
        f.write(("Target file: %s\n" % (self.target_file)).encode('ascii'))
        f.write(b"\n")
        f.write(self.diffstat)
        f.write(b"\n")

    def create_diff_file(self, filename, compressed = False):
        self.logger.debug("Writing debdiff to file")
        if compressed:
            f = gzip.open(filename, 'w')
        else:
            f = open(filename, 'wb')
        try:
            self._print_header(f)
            self.write_to_file(f)
        finally:
            f.close()
