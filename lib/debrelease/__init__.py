## vim:set noet ts=4 sw=4:
#
# (C) 2007-2008 Philipp Kern <pkern@debian.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

import os.path
import os
import sys
import logging
from collections import defaultdict


from debian.deb822 import Deb822

# in minutes
IGNORABLE_TIME_DELTA = 120

logging.getLogger('debrelease').addHandler(logging.NullHandler())

class FileNotFoundException(Exception):
    def __init__(self, filename):
        self.filename = filename
    def __str__(self):
        return "File not found: %s" % (self.filename,)

def tree():
    return defaultdict(tree)

def strip_epoch(version):
    """Strips away the epoch of a version number because we cannot rely on its
    presence in various places."""
    if ':' in version:
        return version.split(':', 1)[1]
    else:
        return version

def normalize_path(path):
    """Normalizes the passed path to the path of the script.  This function is
    used to complete relative directories specified in the configuration
    file."""
    return os.path.normpath(
        os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), path))

def extract_epoch(version):
    """Extracts and returns the epoch of a version number.  If the version does
    not contain an epoch, returns None."""
    if ':' in version:
        return "%s:" % (version.split(':', 1)[0])
    else:
        return None

def create_symlinks(dsc_file, link_dir, pools, projectb):
    # caller is responsible for creating and cleaning up
    # "link_dir"
    with open(dsc_file, encoding='utf-8') as dsc_handler:
        dsc = Deb822(dsc_handler)
        os.symlink(dsc_file, os.path.join(link_dir,
                   os.path.basename(dsc_file)))
        dsc_dir = os.path.dirname(dsc_file)
        files = set(dsc['Files'].split('\n'))
        for files_entry in files:
            if files_entry == '':
                continue
            file = files_entry.rsplit(' ', 1)[1]
            source_file = os.path.join(dsc_dir, file)
            link = os.path.join(link_dir, file)
            if not os.path.exists(link):
                if not os.path.exists(source_file):
                    for pool in pools:
                        _source_file = '%s/%s' % (pool,
                                        projectb.get_pool_file(file))
                        if os.path.exists(_source_file):
                            source_file = _source_file
                            break
                os.symlink(source_file, link)

def locate_file(filename, queue_directory, pools):
    if filename.startswith('/'):
        pass
    elif filename.find('/') == -1:
        filename = "%s/%s" % (queue_directory, filename)
    else:
        for pool in pools:
            _filename = "%s/%s" % (pool, filename)
            if os.path.exists(_filename):
                filename = _filename
                break
    return filename
