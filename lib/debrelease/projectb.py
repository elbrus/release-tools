## vim:set et ts=4 sw=4 cindent:
#
# projectb.py: wrap access to the ProjectB database for the Release Team tools
#
# (C) 2007-2012 Philipp Kern <pkern@debian.org>
# Copyright 2009-2017 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.
#
# Dependencies (expressed in Debian packages):
#  * python-psycopg2 (for projectb access)

import psycopg2 as pg
import psycopg2.extras
import apt_pkg
import re
import logging

from functools import cmp_to_key

from debrelease import strip_epoch

class ProjectB(object):
    def __init__(self):
        self.logger = logging.getLogger('debrelease.projectb')
        self.logger.addHandler(logging.NullHandler())
        self.logger.debug('ProjectB: __init__')

        self.conn = pg.connect('service=projectb')
        self.conn.set_client_encoding('UTF8')
        self.conn.set_session(readonly=True, autocommit=True)
        self.cache = {}
        apt_pkg.init_system()
        self.vercmp = cmp_to_key(apt_pkg.version_compare)

    def get_connection(self):
        return self.conn

    package_query = \
            """select distinct
                   b.package binpkg, b.type bintype, b.version fullbinver,
                   substring(b.version from position(':' in b.version) + 1) binver,
                   a.arch_string, (component.name || '/'::text) || files.filename as filename,
                   src.source, src.version fullsourcever, suite.suite_name suite,
                   substring(src.version from position(':' in src.version) + 1) sourcever
               from"""

    source_package_query = \
           package_query + \
             """
                   source src
                   left join binaries b on b.source = src.id
                   join bin_associations ba on b.id = ba.bin
                   join suite on ba.suite = suite.id
                   join files on b.file = files.id
                   join files_archive_map fam on files.id = fam.file_id
                   join component on fam.component_id = component.id
                   join archive on fam.archive_id = archive.id
                   join architecture a on a.id = b.architecture
                   left join src_associations sa on src.id = sa.source
                where
                   archive.name in ('ftp-master', 'debian-debug', 'policy') and
                   src.source = '%s'"""

    binary_package_query = \
           package_query + \
            """
                   binaries b
                   join bin_associations ba on b.id = ba.bin
                   join suite on ba.suite = suite.id
                   join files on b.file = files.id
                   join files_archive_map fam on files.id = fam.file_id
                   join component on fam.component_id = component.id
                   join archive on fam.archive_id = archive.id
                   join architecture a on a.id = b.architecture
                   left join source src on src.id = b.source
                   join src_associations sa on src.id = sa.source and sa.suite = suite.id
               where
                  archive.name in ('ftp-master', 'debian-debug', 'policy') and
                  b.package = '%s'"""

    def ensure_cached(self, package, suite, source=True):
        self.logger.debug("projectb::ensure_cached: %s %s %s", package, suite, source)
        if suite not in self.cache:
            self.cache[suite] = set()
        if source:
            packages = [package]
        else:
            packages = self.get_source_pkgs(suite, package)
        for this_package in packages:
            if not any(item for item in self.cache[suite] if item.source == this_package):
                self.logger.debug("caching %s", this_package)
                query = self.source_package_query % (this_package)
                query = query + " and suite.suite_name in ('%s', '%s-debug')" % (suite, suite)
                cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
                cursor.execute(query)
                result = cursor.fetchall()
                self.cache[suite] |= set(result)

    def get_architectures(self, suite, srcpkg, version=None, binversion=False):
        self.logger.debug("projectb::get_architectures: %s %s %s %s", suite, srcpkg, version, binversion)
        self.ensure_cached(srcpkg, suite)
        return set(item.arch_string for item in self.cache[suite] if item.source==srcpkg and (
          version is None
          or (
              (binversion and item.binver==version)
              or
              (not binversion and item.sourcever==version)
             )
        ))

    version_query = \
        """select source.version from source join src_associations on source.id
        = src_associations.source join suite on src_associations.suite =
        suite.id where source.source = '%s' and  suite.suite_name = '%s'
        order by source.version desc"""

    def get_version(self, suite, srcpkg, expected=False, epoch=False):
        self.logger.debug("projectb::get_version %s %s %s %s", suite, srcpkg, expected, epoch)
        query = self.version_query % (srcpkg, suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if expected:
            assert result
        if result:
            if epoch:
                return result[0][0]
            else:
                return strip_epoch(result[0][0])
        else:
            return None

    # XXX: This might get wrong results due to messed up dsc/source overrides
    component_query = \
        """select component.name from component join override on
        override.component = component.id join override_type on override_type.id
        = override.type join suite on override.suite = suite.id where
        override.package = '%s' and suite.suite_name = '%s'
        and override_type.type = '%s'"""

    def get_component(self, suite, srcpkg, expected = False):
        self.logger.debug("projectb::get_component %s %s %s", suite, srcpkg, expected)
        query = self.component_query % (srcpkg, suite, 'dsc')
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if expected:
            assert result
        if result:
            return result[0][0]
        else:
            return None

    dsc_file_query = \
        """
            SELECT (component.name || '/'::text) || files.filename AS filename
            FROM source
            INNER JOIN dsc_files ON dsc_files.source = source.id
            INNER JOIN files ON dsc_files.file = files.id
            INNER JOIN files_archive_map fam ON files.id = fam.file_id
            INNER JOIN component ON fam.component_id = component.id
            INNER JOIN archive ON fam.archive_id = archive.id
            WHERE archive.name IN ('ftp-master', 'debian-debug', 'policy') AND
              files.filename like '%%.dsc' AND
              source.source = '%s' AND source.version = '%s'
        """

    def get_dsc_file(self, srcpkg, version):
        self.logger.debug("projectb::get_dsc_file %s_%s", srcpkg, version)
        if re.search(r'\+b\d+$', version):
            raise ValueError('Cannot fetch dsc for binNMUs (%s_%s)'
                % (srcpkg, version))
        query = self.dsc_file_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    pool_file_query = \
       """select (component.name || '/'::text) || files.filename as filename
          from files join files_archive_map fam on files.id = fam.file_id
          join component on fam.component_id = component.id
          join archive on fam.archive_id = archive.id
          where archive.name in ('ftp-master', 'debian-debug', 'policy') and
          (filename like '%%/%s' or filename='%s')"""

    def get_pool_file(self, filename):
        self.logger.debug("projectb::get_pool_file %s", filename)
        query = self.pool_file_query % (filename)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    def get_current_suite_version(self, suite):
        self.logger.debug("projectb::get_current_suite_version %s", suite)
        query = "select version from suite where suite_name = '%s'" \
            % (suite,)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return result[0][0]

    def get_binaries(self, suite, srcpkg, arch=None, version=None):
        self.logger.debug("projectb::get_binaries %s %s %s %s", suite, srcpkg, arch, version)
        self.ensure_cached(srcpkg, suite)
        return set(item.binpkg for item in self.cache[suite] if item.source==srcpkg and
            arch in [None, item.arch_string] and version in [None, item.fullsourcever])

    def get_binaries_for_version(self, srcpkg, version):
        self.logger.debug("projectb::get_binaries_for_version %s_%s", srcpkg, version)
        query = self.source_package_query % (srcpkg)
        query = query + " and src.version = '%s'" % (version)
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def get_binary_version(self, suite, binpkg, arch, epoch=False):
        self.logger.debug("projectb::get_binary_version %s %s/%s %s", suite, binpkg, arch, epoch)
        self.ensure_cached(binpkg, suite, source=False)
        if epoch:
            result = [ item.fullbinver for item in self.cache[suite]
                       if item.binpkg==binpkg and item.arch_string==arch ]
        else:
            result = [ item.binver for item in self.cache[suite]
                       if item.binpkg==binpkg and item.arch_string==arch ]
        if result:
            return max(result, key=self.vercmp)
        else:
            return None

    def get_binary_filename(self, suite, srcpkg, binpkg, arch):
        self.logger.debug("projectb::get_binary_filename %s %s %s %s", suite, srcpkg, binpkg, arch)
        self.ensure_cached(srcpkg, suite)
        result = { item.fullbinver: item.filename for item in self.cache[suite]
                   if item.binpkg==binpkg and item.source==srcpkg and item.arch_string==arch }
        if result:
            return result[max(result, key=self.vercmp)]
        else:
            return None

    is_source_pkg_query = \
        """select count(*) from source join src_associations on source.id
        = src_associations.source join suite on src_associations.suite =
        suite.id where source.source = '%s' and suite.suite_name = '%s'"""

    def is_source_pkg(self, suite, pkgname):
        self.logger.debug("projectb::is_source_pkg %s %s", suite, pkgname)
        query = self.is_source_pkg_query % (pkgname, suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result[0][0] > 0:
            return True

        return False

    get_source_pkg_query = \
        """SELECT DISTINCT source.source FROM binaries
            JOIN source ON binaries.source = source.id
            JOIN src_associations ON source.id = src_associations.source
            JOIN suite ON src_associations.suite = suite.id
           WHERE binaries.package = '%s'
             AND suite.suite_name = '%s'"""

    def get_source_pkgs(self, suite, pkgname):
        self.logger.debug("projectb::get_source_pkgs %s %s", suite, pkgname)
        query = self.get_source_pkg_query % (pkgname, suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return [item[0] for item in result]

        return []

    def get_source_pkg(self, suite, pkgname):
        self.logger.debug("projectb::get_source_pkg %s %s", suite, pkgname)
        result = self.get_source_pkgs(suite, pkgname)
        if result:
            return result[0]

        return None

    def has_udebs(self, suite, srcpkg):
        self.logger.debug("projectb::has_udebs %s %s", suite, srcpkg)
        self.ensure_cached(srcpkg, suite)
        return any(item for item in self.cache[suite] if item.source==srcpkg and item.bintype=='udeb')

    def is_udeb_only(self, suite, srcpkg):
        self.logger.debug("projectb::is_udeb_only %s %s", suite, srcpkg)
        self.ensure_cached(srcpkg, suite)
        binaries = [item for item in self.cache[suite] if item.source==srcpkg]
        return binaries and all(item.bintype=='udeb' for item in binaries)

    changelog_query = \
        """SELECT changelog FROM changelogs
           WHERE source = '%s'
             AND version = '%s'"""

    def get_changelog(self, srcpkg, version):
        self.logger.debug("projectb::get_changleog %s_%s", srcpkg, version)
        query = self.changelog_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return result[0][0]

        return ''

    BUGS_CLOSED_RE=re.compile(
        r'closes:\s*(?:bug)?\#?\s?\d+(?:,\s*(?:bug)?\#?\s?\d+)*',
        re.IGNORECASE | re.DOTALL)

    def get_bugs_closed(self, srcpkg, version):
        self.logger.debug("projectb::get_bugs_closed %s_%s", srcpkg, version)
        changelog = self.get_changelog(srcpkg, version)
        bugs = set()
        for m in self.BUGS_CLOSED_RE.findall(changelog):
            bugs.update(re.findall(r'\d+', m))
        return bugs

    changes_files_for_policy_queue_query = \
        """SELECT changes.changesname, changes.id
           FROM changes
           INNER JOIN policy_queue_upload uploads ON changes.id=uploads.changes_id
           INNER JOIN policy_queue queue on uploads.policy_queue_id = queue.id
           WHERE queue_name = '%s'
        """

    def get_changes_files_for_policy_queue(self, suite):
        self.logger.debug("projectb::get_changes_files_for_policy_queue %s", suite)
        query = self.changes_files_for_policy_queue_query % (suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return set(((row[0], row[1]) for row in result))

    changes_details_query = \
        """ SELECT changesname, source, binaries, architecture, version,
               distribution, urgency, maintainer, fingerprint, changedby,
               date, closes, seen
            FROM changes
            WHERE id=%s
        """

    def get_changes_details(self, changesid):
        self.logger.debug("projectb::get_changes_details %s", changesid)
        query = self.changes_details_query % (changesid)
        cursor = self.conn.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
        cursor.execute(query)
        result = cursor.fetchall()
        if result:
            return result[0]
        else:
            return None

    # This may give incorrect results if the same file is in
    # different components in different archives
    policy_binaries_from_changesid_query = \
        """
            SELECT binaries.package, binaries.version, arch_string architecture,
              (component.name || '/'::text) || files.filename
            FROM changes
            INNER JOIN policy_queue_upload uploads ON changes.id=uploads.changes_id
            INNER JOIN policy_queue_upload_binaries_map binmap ON uploads.id = binmap.policy_queue_upload_id
            INNER JOIN binaries ON binmap.binary_id = binaries.id
            INNER JOIN architecture ON binaries.architecture = architecture.id
            INNER JOIN source ON binaries.source = source.id
            INNER JOIN files ON binaries.file = files.id
            INNER JOIN files_archive_map fam ON files.id = fam.file_id
            INNER JOIN component ON component.id=fam.component_id
            WHERE changes_id=%s
        """

    def get_binaries_for_policy_queue_by_changes(self, changesid):
        self.logger.debug("projectb::get_binaries_for_policy_queue_by_changes %s", changesid)
        query = self.policy_binaries_from_changesid_query % (changesid)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return set(((row[0], row[1], row[2], row[3]) for row in result))

    is_valid_suite_name_query = """SELECT 1 FROM suite WHERE suite_name = '%s' LIMIT 1"""

    def is_valid_suite_name(self, suite):
        self.logger.debug("projectb::is_valid_suite_name %s", suite)
        query = self.is_valid_suite_name_query % (suite)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        return len(result) > 0

    # Abuse LEFT JOINs to get 1 result if the version exists but is not in a suite
    # (makes the suite name field empty)
    check_version_query = \
        """
           SELECT 1, suite.suite_name FROM source
                  LEFT JOIN src_associations sa ON source.id = sa.source
                  LEFT JOIN suite ON sa.suite = suite.id
           WHERE source.source = '%s' AND source.version = '%s'
           LIMIT 1
        """

    def check_version(self, srcpkg, version):
        self.logger.debug("projectb::check_version %s_%s", srcpkg, version)
        query = self.check_version_query % (srcpkg, version)
        cursor = self.conn.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        if len(result) < 1:
            return (False, None)
        suite = result[0][1]
        if suite == '':
            suite = None
        return (True, suite)
