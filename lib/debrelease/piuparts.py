## vim:set et ts=4 sw=4:
#
# piuparts.py: read lists of the piuparts status of packages in a
#              suite
#
# (C) Copyright 2015 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

from __future__ import with_statement
import os.path

class PiupartsFactory(object):
    def __init__(self, config, section):
        self.statuses_base = config.get('piuparts', 'directory')

        self.codename = config.get(section, 'codename')

        self.statuses = dict()
        self._load_statuses('-pu', 'install')
        self._load_statuses('2proposed', 'upgrade')

    def _load_statuses(self, suffix, type):
        """
        The input files we read are stored in the directory defined in
        the config file (section `piuparts', key `directory') with the name
        `suite-pu' (for install tests) or `suite2proposed` (for tests of
        upgrades from the base suite.  Each file contains one line per source
        package, in the form "srcpkg: status".
        """

        filename = os.path.join(self.statuses_base, '%s%s' % (self.codename, suffix))
        statuses = dict()
        with open(filename, 'r') as f:
            for line in f:
                entry = line.strip().split(': ')
                srcpkg, status = entry[0], entry[1]
                statuses[srcpkg] = status
        self.statuses[type] = statuses

    def __getitem__(self, type=None):
        if type is not None and type in self.statuses:
            return self.statuses[type]
